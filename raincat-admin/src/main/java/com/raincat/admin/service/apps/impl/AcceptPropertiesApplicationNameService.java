package com.raincat.admin.service.apps.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.raincat.admin.service.apps.AcceptApplicationNameService;
import com.raincat.admin.service.apps.enums.AcceptApplicationNameEnum;

/**
 * @author chaoscoffee
 * @date 2018/7/10
 * @description
 */
@Service
public class AcceptPropertiesApplicationNameService implements AcceptApplicationNameService {

    @Value("${recover.application.list}")
    private List<String> appNameList;

    @Override
    public <T> List<T> acceptAppNameList(List<T> apps) {
        if (CollectionUtils.isNotEmpty(appNameList)) {
            appNameList.forEach(app -> apps.add((T) app));
        }
        return apps;
    }

    @Override
    public AcceptApplicationNameEnum code() {
        return AcceptApplicationNameEnum.PROPERTY;
    }
}
